﻿using dndhelper.models;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace dndhelper
{
    public class MainModel
    {
        public ObservableCollection<CharacterModel> Foes { get; set; } = new ObservableCollection<CharacterModel>();
        public ObservableCollection<CharacterModel> Friends { get; set; } = new ObservableCollection<CharacterModel>();
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainModel _model;
        public MainWindow()
        {
            InitializeComponent();

            DataContext = _model = new MainModel();

            var z = new models.PlayerModel();
        }

        private void AddEnemy_Click(object sender, RoutedEventArgs e)
        {
            var popup = new CreateCreature(new models.CharacterModel());
            if(popup.ShowDialog() == true)
            {
                var card = new PlayerCard(_model);
                card.DataContext = popup.Model;
                _model.Foes.Add(popup.Model);
            }
        }

        private void AddAlly_Click(object sender, RoutedEventArgs e)
        {
            var popup = new CreateCreature(new models.PlayerModel());
            if (popup.ShowDialog() == true)
            {
                var card = new PlayerCard(_model);
                card.DataContext = popup.Model;
                _model.Friends.Add(popup.Model);
            }
        }

        private void SaveState_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog() { DefaultExt = ".json" };
            if (saveFileDialog.ShowDialog() != true)
                return;

            var isAllies = (sender as Control).Tag != null;

            var collection = isAllies ? _model.Friends : _model.Foes;

            var path = Path.GetDirectoryName(saveFileDialog.FileName);

            foreach(var creature in collection)
            {
                if(string.IsNullOrWhiteSpace(creature.FileName) || !File.Exists(creature.FileName))
                {
                    creature.FileName = Path.Combine(path, Path.GetTempFileName());
                    var creatureJson = JsonConvert.SerializeObject(creature);
                    File.WriteAllText(creature.FileName, creatureJson);
                }
            }

            var toSerialize = collection.Select(x => new
            {
                FileName = x.FileName,
                Name = x.Name,
                CurrentHP = x.CurrentHP,
                Modifiers = x.BonusesAndPenalties
            }).ToArray();

            var json = JsonConvert.SerializeObject(toSerialize);
                File.WriteAllText(saveFileDialog.FileName, json);
        }

        private void LoadState_Click(object sender, RoutedEventArgs e)
        {
            var isAllies = (sender as Control).Tag != null;

            StringBuilder errors = new StringBuilder();

            OpenFileDialog openFileDialog = new OpenFileDialog() { DefaultExt = ".json" };
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var text = File.ReadAllText(openFileDialog.FileName);

                    var dummyType = new[]
                    {
                        new
                        {
                            FileName = "",
                            Name = "",
                            CurrentHP = 0,
                            Modifiers = new Dictionary<eStats, List<StatBonusOrPenalty>>()
                        }
                    };

                    var creatures = JsonConvert.DeserializeAnonymousType(text, dummyType);

                    var collection = isAllies ? _model.Friends : _model.Foes;

                    collection.Clear();
                    foreach (var creature in creatures)
                    {
                        if(!File.Exists(creature.FileName))
                        {
                            errors.AppendLine($"Could not load {creature.FileName} ({creature.Name})");
                            continue;
                        }

                        var baseCreatureText = File.ReadAllText(creature.FileName);
                        var creatureModel = JsonConvert.DeserializeObject<CharacterModel>(baseCreatureText);

                        creatureModel.FileName = creature.FileName;
                        creatureModel.Name = creature.Name;
                        creatureModel.CurrentHP = creature.CurrentHP;
                        creatureModel.BonusesAndPenalties = creature.Modifiers;

                        var card = new PlayerCard(_model);
                        card.DataContext = creatureModel;
                        collection.Add(creatureModel);
                    }

                    if(errors.Length > 0)
                        MessageBox.Show(errors.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Something went wrong: {ex}");
                }
            }
        }
        private void LoadStateLegacy_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog() { DefaultExt = ".json" };
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var text = File.ReadAllText(openFileDialog.FileName);

                    var model = JsonConvert.DeserializeObject<MainModel>(text);

                    _model.Friends.Clear();
                    foreach (var friend in model.Friends)
                    {
                        var card = new PlayerCard(_model);
                        card.DataContext = friend;
                        _model.Friends.Add(friend);
                    }

                    _model.Foes.Clear();
                    foreach (var foe in model.Foes)
                    {
                        var card = new PlayerCard(_model);
                        card.DataContext = foe;
                        _model.Foes.Add(foe);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Something went wrong: {ex}");
                }
            }
        }
    }

    public class TrueToVisibleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                var boolValue = (bool)value;
                return boolValue ? Visibility.Visible : Visibility.Collapsed;
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is Visibility)
            {
                var enumValue = (Visibility)value;

                return enumValue == Visibility.Visible;
            }

            return false;
        }
    }
    public class ZeroToNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is int)
            {
                var intVal = (int)value;
                if (intVal == 0)
                    return null;
            }

            if (value is int?)
            {
                var intVal = (int?)value;
                if (intVal == 0)
                    return null;
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }

    public class eStatsNoneToNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return eStats.NONE;

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is eStats)
            {
                var eStatsVal = (eStats)value;
                if (eStatsVal == eStats.NONE)
                    return null;
            }

            if (value is eStats?)
            {
                var eStatsVal = (eStats?)value;
                if (eStatsVal == eStats.NONE)
                    return null;
            }

            return value;
        }
    }
}
