﻿using dndhelper.models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dndhelper
{
    /// <summary>
    /// Interaction logic for AddModifierPopup.xaml
    /// </summary>
    public partial class AddModifierPopup : Window
    {
        public StatBonusOrPenalty Modifier { get { return DataContext as StatBonusOrPenalty; } }

        public AddModifierPopup()
        {
            DataContext = new StatBonusOrPenalty();

            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
