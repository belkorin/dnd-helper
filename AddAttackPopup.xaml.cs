﻿using dndhelper.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dndhelper
{
    /// <summary>
    /// Interaction logic for AddAttackPopup.xaml
    /// </summary>
    public partial class AddAttackPopup : Window
    {
        public bool IsRanged { get; set; }
        public bool IsMelee { get { return !IsRanged; } }

        public Attack Attack { get; private set; }
        public AttackAddon SelectedAddon { get; set; }

        public AddAttackPopup(bool isRanged)
        {
            IsRanged = isRanged;

            Attack = new Attack();
            DataContext = Attack;

            InitializeComponent();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void PlusButton_Click(object sender, RoutedEventArgs e)
        {
            var popup = new AddAttackAddonPopup();
            if (popup.ShowDialog() != true)
                return;

            Attack.AttackAddons = Attack.AttackAddons.Concat(new[] { popup.AttackAddon }).ToArray();
            Attack.NotifyAddonsChanged();
        }

        private void MinusButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedAddon == null)
                return;

            var addon = SelectedAddon;
            SelectedAddon = null;

            Attack.AttackAddons = Attack.AttackAddons.Except(new[] { addon }).ToArray();
            Attack.NotifyAddonsChanged();
        }
    }

    public class DesignTimeAttackItem : Attack
    {
        public DesignTimeAttackItem()
        {
            Description = "Sample melee atk";
            Suffix = "suffix";
            AdditionalSuffix = "suffix2";
            AttackAddons = new[]
            {
                new AttackAddon
                {
                    AdditionalDamageBonus = 4,
                    DamageStat = eStats.WIS,
                    NumberOfDice = 1,
                    SidesOfDice = 6,
                    Suffix = "fire damage"
                }
            };
        }
    }
}
