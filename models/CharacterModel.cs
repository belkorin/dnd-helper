﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace dndhelper.models
{
    public enum eStats { STR = 0, DEX, CON, INT, WIS, CHA, BAB, INIT, AC, TOUCHAC, FORT, REF, WILL, DMG, CMD, CMB, TEMPHP, NONE }
    public enum eBonusSources { Alchemical, Armor, Circumstance, Competence, Deflection, Dodge, Enhancement, Insight, Luck, Morale, Profane, Resistance, Sacred, Untyped, TempHP }

    public enum SizeCategories { Fine = 8, Diminutive = 4, Tiny = 2, Small = 1, Medium = 0, Large = -1, Huge = -2, Gargantuan = -4, Colossal = -8 }

    public class CharacterModel : INotifyPropertyChanged
    {
        public string FileName { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        Dictionary<eStats, (int old, int pending)> _pendingUpdates = new Dictionary<eStats, (int old, int pending)>();

        public virtual string Name { get; set; }
        public virtual int HP { get; set; }
        public virtual int TempHP { get { return GetBonuses(eStats.TEMPHP); } }
        public bool HasTempHP { get { return TempHP > 0; } }
        public virtual int HD { get; set; }
        public virtual int CurrentHP { get; set; }
        public virtual int ArmorBonus { get; set; }
        public virtual int NaturalArmorBonus { get; set; }
        public virtual int DodgeBonus { get; set; }
        public virtual int ShieldBonus { get; set; }

        [JsonIgnore]
        public virtual int OtherACBonus
        { 
            get 
            { 
                return GetBonuses(eStats.AC); 
            } set
            {
                _bonusesAndPenalties[eStats.AC] = new List<StatBonusOrPenalty>() { new StatBonusOrPenalty { Amount = value, Description = $"unknown {eStats.AC} bonus", Modifies = eStats.AC } };
            } 
        }

        public virtual SizeCategories SizeCategory { get; set; }
        public virtual int Space { get; set; }
        public virtual int Reach { get; set; }

        public virtual int SizeModifier { get { return (int)SizeCategory; } }

        [JsonIgnore]
        public virtual int InitMod
        {
            get
            {
                return DexMod + GetBonuses(eStats.INIT);
            }
            set
            {
                SetGenericBonus(eStats.INIT, value, InitMod);
            }
        }
        [JsonIgnore]
        public int InitModTemp
        {
            get
            {
                return _pendingUpdates.GetValueOrDefault(eStats.INIT, (InitMod, InitMod)).Item2;
            }
            set
            {
                _pendingUpdates[eStats.INIT] = (InitMod, value);
            }
        }

        public int MaxDexToAC { get; set; }

        public int DexModToAC { get { return Math.Min(MaxDexToAC, DexMod); } }

        public virtual int AC { get { return 10 + ArmorBonus + NaturalArmorBonus + ShieldBonus + SizeModifier + DexModToAC + DodgeBonus + GetBonuses(eStats.AC) + GetBonuses(eStats.TOUCHAC); } }
        public virtual int FlatFooted { get { return AC - DexModToAC - DodgeBonus - ((_bonusesAndPenalties.GetValueOrDefault(eStats.AC)?.Where(x => x.BonusSource == eBonusSources.Dodge).Sum(x => x.Amount)) ?? 0); } }

        [JsonIgnore]
        public virtual int TouchAC 
        { 
            get 
            { 
                return 10 + SizeModifier + DexModToAC + DodgeBonus + GetBonuses(eStats.TOUCHAC);
            }
            set
            {
                SetGenericBonus(eStats.TOUCHAC, value, TouchAC);
            }
        }
        [JsonIgnore]
        public int TouchACTemp
        {
            get
            {
                return _pendingUpdates.GetValueOrDefault(eStats.TOUCHAC, (TouchAC, TouchAC)).Item2;
            }
            set
            {
                _pendingUpdates[eStats.TOUCHAC] = (TouchAC, value);
            }
        }

        [JsonIgnore]
        public virtual int CMD 
        {
            get 
            { 
                return 10 + BAB + GetStatMod(eStats.STR) + GetStatMod(eStats.DEX) - SizeModifier + DodgeBonus + GetCMDBonus(); 
            }
            set
            {
                SetGenericBonus(eStats.CMD, value, CMD);
            }
        }
        [JsonIgnore]
        public int CMDTemp
        {
            get
            {
                return _pendingUpdates.GetValueOrDefault(eStats.CMD, (CMD, CMD)).Item2;
            }
            set
            {
                _pendingUpdates[eStats.CMD] = (CMD, value);
            }
        }

        [JsonIgnore]
        public virtual int CMB 
        { 
            get 
            { 
                return BAB + GetStatMod(eStats.STR) - SizeModifier + GetBonuses(eStats.CMB); 
            }
            set
            {
                SetGenericBonus(eStats.CMB, value, CMB);
            }
        }
        [JsonIgnore]
        public int CMBTemp
        {
            get
            {
                return _pendingUpdates.GetValueOrDefault(eStats.CMB, (CMB, CMB)).Item2;
            }
            set
            {
                _pendingUpdates[eStats.CMB] = (CMB, value);
            }
        }


        public virtual int FortSaveBase { get; set; }
        public virtual eStats FortStat { get; set; } = eStats.CON;
        [JsonIgnore]
        public virtual int FortSave 
        { 
            get 
            { 
                return FortSaveBase + GetStatMod(FortStat) + GetBonuses(eStats.FORT); 
            } 
            set 
            {
                FortSaveBase = value - GetStatMod(FortStat) - GetBonuses(eStats.FORT); 
            } 
        }

        public virtual int RefSaveBase { get; set; }
        public virtual eStats RefStat { get; set; } = eStats.DEX;
        [JsonIgnore]
        public virtual int RefSave
        {
            get
            {
                return RefSaveBase + GetStatMod(RefStat) + GetBonuses(eStats.REF);
            }
            set
            {
                RefSaveBase = value - GetStatMod(RefStat) - GetBonuses(eStats.REF);
            }
        }
        public virtual int WillSaveBase { get; set; }
        public virtual eStats WillStat { get; set; } = eStats.WIS;
        [JsonIgnore]
        public virtual int WillSave
        {
            get
            {
                return WillSaveBase + GetStatMod(WillStat) + GetBonuses(eStats.WILL);
            }
            set
            {
                WillSaveBase = value - GetStatMod(WillStat) - GetBonuses(eStats.WILL);
            }
        }

        public virtual string Resistances { get; set; }
        public virtual string Immunities { get; set; }
        public virtual string Weaknesses { get; set; }
        public int DRInt { get; set; }
        public string DRBypass { get; set; }
        public virtual string DR { get { return DRInt == 0 ? "" : $"DR {DRInt}/{DRBypass}"; } }
        public virtual string SR { get; set; }

        [JsonIgnore]
        public bool HasAdditionalDefenses { get { return !string.IsNullOrWhiteSpace(Resistances + Immunities + DR + SR + Weaknesses); } }
        [JsonIgnore]
        public bool HasResistOrImmune { get { return !string.IsNullOrWhiteSpace(Resistances + Immunities); } }
        [JsonIgnore]
        public bool HasResist { get { return !string.IsNullOrWhiteSpace(Resistances); } }
        [JsonIgnore]
        public bool HasImmune { get { return !string.IsNullOrWhiteSpace(Immunities); } }
        [JsonIgnore]
        public bool HasDRorSR { get { return !string.IsNullOrWhiteSpace(DR + SR); } }
        [JsonIgnore]
        public bool HasWeakness { get { return !string.IsNullOrWhiteSpace(Weaknesses); } }

        public virtual string SpecialTraits { get; set; }

        public int[] Stats { get { return _stats; } set { _stats = value; } }
        public Dictionary<eStats, List<StatBonusOrPenalty>> BonusesAndPenalties { get { return _bonusesAndPenalties; } set { _bonusesAndPenalties = value; } }
        public StatBonusOrPenalty[] BonusesAndPenaltiesFlattened { get { return _bonusesAndPenalties.SelectMany(x => x.Value.Where(y => y.Amount != 0)).ToArray(); } }

        protected int[] _stats = new int[16];
        protected Dictionary<eStats, List<StatBonusOrPenalty>> _bonusesAndPenalties = new Dictionary<eStats, List<StatBonusOrPenalty>>();
        [JsonIgnore]
        public int Str { get { return GetStat(eStats.STR); } set { SetStat(eStats.STR, value);}}
        [JsonIgnore]
        public int Dex { get { return GetStat(eStats.DEX); } set { SetStat(eStats.DEX, value);} }
        [JsonIgnore]
        public int Con { get { return GetStat(eStats.CON); } set { SetStat(eStats.CON, value);} }
        [JsonIgnore]
        public int Int { get { return GetStat(eStats.INT); } set { SetStat(eStats.INT, value);} }
        [JsonIgnore]
        public int Wis { get { return GetStat(eStats.WIS); } set { SetStat(eStats.WIS, value);} }
        [JsonIgnore]
        public int Cha { get { return GetStat(eStats.CHA); } set { SetStat(eStats.CHA, value); } }
        [JsonIgnore]
        public int BAB { get { return GetStat(eStats.BAB); } set { SetStat(eStats.BAB, value); } }

        public int StrMod { get { return GetStatMod(eStats.STR); } }
        public int DexMod { get { return GetStatMod(eStats.DEX); } }
        public int ConMod { get { return GetStatMod(eStats.CON); } }
        public int IntMod { get { return GetStatMod(eStats.INT); } }
        public int WisMod { get { return GetStatMod(eStats.WIS); } }
        public int ChaMod { get { return GetStatMod(eStats.CHA); } }

        public string MoveSpeed { get; set; }
        public string Senses { get; set; }

        public Attack[] Attacks { get; set; } = new Attack[0];
        public bool HasMelee => Attacks?.Any(x => !x.RangeIncrement.HasValue) ?? false;
        public bool HasRanged => Attacks?.Any(x => x.RangeIncrement.HasValue) ?? false;
        public string MeleeAttackDescription { get { return HasMelee ? $"Melee: {GetAttacks(false)}" : ""; } }
        public string RangedAttackDescription { get { return HasRanged ? $"Ranged: {GetAttacks(true)}" : ""; } }

        public string SpecialAbilities { get; set; }
        public string Notes { get; set; }

        public RelevantSkill[] RelevantSkills { get; set; } = new RelevantSkill[0];
        public string RelevantSkillsString { get { return RelevantSkills.Any() ? $"Skills: {(string.Join(", ", RelevantSkills.Select(x => x.RenderToString(this))))}" : ""; } }

        public virtual int GetStat(eStats stat)
        {
            return _stats[(int)stat] + GetBonuses(stat);
        }

        void SetStat(eStats stat, int statValue)
        {
            _stats[(int)stat] = statValue;
        }

        public virtual int GetStatMod(eStats? stat)
        {
            if (stat == null)
                return 0;

            return GetMod(GetStat(stat.Value));
        }

        public int GetBonuses(eStats stat)
        {
           return (_bonusesAndPenalties.GetValueOrDefault(stat)?.Sum(x => x.Amount) ?? 0);
        }

        public int GetCMDBonus()
        {
            var cmdBonuses = GetBonuses(eStats.CMD);
            var relevantACBonuses = _bonusesAndPenalties.GetValueOrDefault(eStats.AC)?
                .Where(x =>
                x.BonusSource != eBonusSources.Alchemical &&
                x.BonusSource != eBonusSources.Armor &&
                x.BonusSource != eBonusSources.Competence &&
                x.BonusSource != eBonusSources.Enhancement &&
                x.BonusSource != eBonusSources.Resistance &&
                x.BonusSource != eBonusSources.Untyped).Sum(x => x.Amount) ?? 0;

            return cmdBonuses + relevantACBonuses;
        }

        void SetGenericBonus(eStats stat, int newTotal, int oldTotal)
        {
            var bonus = newTotal - oldTotal + GetBonuses(stat);
            _bonusesAndPenalties[stat] = new List<StatBonusOrPenalty>() { new StatBonusOrPenalty { Amount = bonus, Description = $"unknown {stat} bonus", Modifies = stat } };
        }

        public void SetSpecificBonus(eStats stat, eBonusSources source, int bonusOrPenalty, string description, int? duration, int? startedOnRound)
        {
            bool mayStack = StatBonusOrPenalty.BonusAllowsStacking(source);

            List<StatBonusOrPenalty> existing;

            if (!_bonusesAndPenalties.TryGetValue(stat, out existing))
                _bonusesAndPenalties[stat] = existing = new List<StatBonusOrPenalty>();

            if(!mayStack)
            {
                var currentBonus = existing.SingleOrDefault(x => x.BonusSource == source);
                var currentBonusAmount = currentBonus?.Amount ?? 0;

                if (currentBonusAmount < bonusOrPenalty)
                    existing.Remove(currentBonus);
            }

            existing.Add(new StatBonusOrPenalty { Amount = bonusOrPenalty, BonusSource = source, Modifies = stat, Description = description, Duration = duration, StartedOnRound = startedOnRound });

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null)); //just refresh everything...
        }

        public void RemoveSpecificBonus(StatBonusOrPenalty bonusOrPenalty)
        {
            if (!_bonusesAndPenalties.TryGetValue(bonusOrPenalty.Modifies, out var bonuses))
                return;

            bonuses.Remove(bonusOrPenalty);

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null)); //just refresh everything...
        }

        public void DoDamageOrHeal(bool bypassDR, int damage)
        {
            if (damage < 0)
            {
                CurrentHP = Math.Min(CurrentHP - damage, HP);
                return;
            }

            int dr = DRInt;
            if (bypassDR)
                dr = 0;

            damage -= dr;

            if (damage <= 0)
                return;

            if(TempHP > 0)
            {
                var remainingTempHP = Math.Max(TempHP - damage, 0);
                var remainingDamage = Math.Max(damage - TempHP, 0);

                var tempHPBonus = _bonusesAndPenalties[eStats.TEMPHP].Single();

                if (remainingTempHP > 0)
                    tempHPBonus.Amount = remainingTempHP;
                else
                    RemoveSpecificBonus(tempHPBonus);

                damage = remainingDamage;
            }

            CurrentHP -= damage;
        }

        public void ApplyPending()
        {
            foreach (var pending in _pendingUpdates)
            {
                switch(pending.Key)
                {
                    case eStats.FORT:
                        break;
                    case eStats.REF:
                        break;
                    case eStats.WILL:
                        break;
                    default:
                        SetGenericBonus(pending.Key, pending.Value.pending, pending.Value.old);
                        break;
                }

            }

            _pendingUpdates.Clear();
        }

        public static int GetMod(int stat)
        {
            return (int)Math.Floor((stat - 10) / 2m);
        }

        string GetAttacks(bool ranged)
        {
            return string.Join("; ", Attacks.Where(x => x.RangeIncrement.HasValue == ranged).Select(x => x.RenderAttackDescription(this)));
        }

        public virtual CharacterModel Clone()
        {
            var clone = (CharacterModel)MemberwiseClone();
            clone.BonusesAndPenalties = this.BonusesAndPenalties?.ToDictionary(x => x.Key, x => x.Value.Select(y => y.Clone()).ToList());
            clone.Attacks = this.Attacks?.Select(x => x.Clone()).ToArray();
            clone.RelevantSkills = this.RelevantSkills?.Select(x => x.Clone()).ToArray();
            clone._stats = this._stats.Select(x => x).ToArray();
            return clone;
        }

        public virtual void ApplyClone(CharacterModel clone)
        {
            this.ArmorBonus = clone.ArmorBonus;
            this.Attacks = clone.Attacks?.Select(x => x.Clone()).ToArray();
            this.BAB = clone.BAB;
            this.BonusesAndPenalties = clone.BonusesAndPenalties?.ToDictionary(x => x.Key, x => x.Value.Select(y => y.Clone()).ToList());
            this.CurrentHP = clone.CurrentHP;
            this.DodgeBonus = clone.DodgeBonus;
            this.DRBypass = clone.DRBypass;
            this.DRInt = clone.DRInt;
            this.FortSaveBase = clone.FortSaveBase;
            this.FortStat = clone.FortStat;
            this.HD = clone.HD;
            this.HP = clone.HP;
            this.Immunities = clone.Immunities;
            this.MaxDexToAC = clone.MaxDexToAC;
            this.MoveSpeed = clone.MoveSpeed;
            this.Name = clone.Name;
            this.NaturalArmorBonus = clone.NaturalArmorBonus;
            this.Notes = clone.Notes;
            this.Reach = clone.Reach;
            this.RefSaveBase = clone.RefSaveBase;
            this.RefStat = clone.RefStat;
            this.RelevantSkills = clone.RelevantSkills?.Select(x => x.Clone()).ToArray();
            this.Resistances = clone.Resistances;
            this.Senses = clone.Senses;
            this.ShieldBonus = clone.ShieldBonus;
            this.SizeCategory = clone.SizeCategory;
            this.Space = clone.Space;
            this.SpecialAbilities = clone.SpecialAbilities;
            this.SpecialTraits = clone.SpecialTraits;
            this.SR = clone.SR;
            this.Stats = clone.Stats.Select(x => x).ToArray();
            this.Weaknesses = clone.Weaknesses;
            this.WillSaveBase = clone.WillSaveBase;
            this.WillStat = clone.WillStat;

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null)); //just refresh everything...
        }

        public CharacterModel() { BonusesAndPenalties = new Dictionary<eStats, List<StatBonusOrPenalty>>(); }

        public CharacterModel(bool design)
        {
            _stats = new int[] { 9, 10, 11, 12, 13, 14, 3 };
            _bonusesAndPenalties = new Dictionary<eStats, List<StatBonusOrPenalty>>();
            Name = "Rodent of unusual size";
            HP = 33;
            HD = 4;
            CurrentHP = 25;
            ArmorBonus = 2;
            ShieldBonus = 1;
            NaturalArmorBonus = 3;
            MaxDexToAC = 10;
            SizeCategory = SizeCategories.Medium;
            FortSave = 5;
            RefSave = 2;
            WillSave = 4;
            MoveSpeed = "30'; Burrow 10'";
            Senses = "Darkvision 60'; Tremorsense 30'";
            Space = 5;
            Reach = 5;

            Notes = "Tactics - hides in the dark and spits acid, mostly";

            Resistances = "Resist 5 acid";
            Immunities = "Vermin traits";
            SR = "SR 15";
            DRInt = 2;
            DRBypass = "-";
            Weaknesses = "light blindness";

            SpecialAbilities = @"Poision
Bite—injury; save Fort DC 11; frequency 1/round for 2 rounds; effect 1d2 Str; cure 1 save. The save DC is Constitution-based.

Sneaky
RoUS can hide on a turn that it attacked without penalty.
";

            RelevantSkills = new[]
            {
                new RelevantSkill
                {
                    SkillName = "Stealth",
                    BonusFromRanksAndOther = 8,
                    RelevantStat = eStats.DEX,
                    ConditionalStuff = "+2 bonus in sewers and swamps"
                },
                new RelevantSkill
                {
                    SkillName = "Intimidate",
                    BonusFromRanksAndOther = 4,
                    RelevantStat = eStats.CHA
                },
                new RelevantSkill
                {
                    SkillName = "Perception",
                    BonusFromRanksAndOther = 6,
                    RelevantStat = eStats.WIS
                }
            };

            Attacks = new[]
            {
                new Attack
                {
                    Description = "Bite",
                    ToHit = 1,
                    ToHitStat = eStats.STR,
                    NumberOfDice = 2,
                    SidesOfDice = 4,
                    DamageStat = eStats.STR,
                    AttackAddons = new []
                    {
                        new AttackAddon
                        {
                            Description = "+",
                            NumberOfDice = 1,
                            SidesOfDice = 6,
                            Suffix = "acid"
                        },
                        new AttackAddon
                        {
                            Description = "+ poision"
                        }
                    }
                },
                new Attack
                {
                    Description = "2 Claws",
                    ToHitStat = eStats.DEX,
                    NumberOfDice = 1,
                    SidesOfDice = 4,
                    DamageStat = eStats.STR
                },
                new Attack
                {
                    Description = "Spit",
                    RangeIncrement = 20,
                    ToHitStat = eStats.DEX,
                    AdditionalDamageBonus = 2,
                    Suffix = "acid"
                }
            };

            SetSpecificBonus(eStats.INIT, eBonusSources.Insight, 1, "Very aware of surroundings", null, null);
            SetSpecificBonus(eStats.STR, eBonusSources.Alchemical, 2, "Potion of strenght", 6, 12);
        }
    }

    public class DesignTimeCharacterModel : CharacterModel
    {
        public DesignTimeCharacterModel() : base(true) { }
    }

    public class StatBonusOrPenalty : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string Description { get; set; }
        public int Amount { get; set; }
        public eStats Modifies { get; set; }
        public eBonusSources BonusSource { get; set; }
        public int? Duration { get; set; }
        public int? StartedOnRound { get; set; }

        public string UIDescription
        {
            get 
            {
                string duration = Duration.HasValue ? $"for {Duration} turns from {StartedOnRound}" : "Doesn't expire";
                return $"{Amount:+0;-#} {BonusSource}  {(Amount < 0 ? "penalty" : "bonus")} to {Modifies} ({Description}) ({duration})";
            }
        }

        public StatBonusOrPenalty()
        {
            BonusSource = eBonusSources.Untyped;
        }

        public StatBonusOrPenalty Clone()
        {
            return (StatBonusOrPenalty)MemberwiseClone();
        }

        public static bool BonusAllowsStacking(eBonusSources source)
        {
            switch(source)
            {
                case eBonusSources.Alchemical:
                case eBonusSources.Armor:
                case eBonusSources.Competence:
                case eBonusSources.Deflection:
                case eBonusSources.Enhancement:
                case eBonusSources.Insight:
                case eBonusSources.Luck:
                case eBonusSources.Profane:
                case eBonusSources.Resistance:
                case eBonusSources.Sacred:
                case eBonusSources.TempHP:
                    return false;
                case eBonusSources.Circumstance:
                case eBonusSources.Dodge:
                case eBonusSources.Morale:
                case eBonusSources.Untyped:
                    return true;
                default:
                    throw new Exception("Unknown bonus type");
            }
        }

    }

    public class Attack : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public static eStats[] ToHitStats = new[] { eStats.STR, eStats.DEX, eStats.CON, eStats.INT, eStats.WIS, eStats.CHA };
        public static eStats[] ToDamageStats = new[] { eStats.NONE, eStats.STR, eStats.DEX, eStats.CON, eStats.INT, eStats.WIS, eStats.CHA };

        public string Description { get; set; }
        public string Suffix { get; set; }
        public string AdditionalSuffix { get; set; }
        public int ToHit { get; set; }
        public int NumberOfDice { get; set; }
        public int SidesOfDice { get; set; }
        public int AdditionalDamageBonus { get; set; }

        public int? RangeIncrement { get; set; }

        public eStats ToHitStat { get; set; }
        public eStats? DamageStat { get; set; }
        public bool StrengthAndAHalf { get; set; }

        public AttackAddon[] AttackAddons { get; set; } = new AttackAddon[0];

        public void NotifyAddonsChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(AttackAddons)));
        }

        public string RenderAttackDescription(CharacterModel chr)
        {
            int toHit = ToHit + chr.GetStat(eStats.BAB) + chr.GetStatMod(ToHitStat) + chr.SizeModifier;

            string rangeIncrement = RangeIncrement.HasValue ? $" ({RangeIncrement}' range)" : "";
            string dice = NumberOfDice == 0 ? "" : $"{NumberOfDice}d{SidesOfDice}";
            string damageBonus = GetDamageBonus(chr);
            string suffix = string.IsNullOrEmpty(Suffix) ? "" : $" ({Suffix})";
            string secondSuffix = string.IsNullOrEmpty(AdditionalSuffix) ? "" : $" ({AdditionalSuffix})";
            string description = string.IsNullOrEmpty(Description) ? "" : $"{Description} ";

            string addons = string.Join(' ', AttackAddons.Select(x => x.RenderAdditionalDescription(chr)));

            return $"{description}{toHit:+0;-#} {dice}{damageBonus}{suffix}{addons}{secondSuffix}{rangeIncrement}";
        }

        string GetDamageBonus(CharacterModel chr)
        {
            if (!DamageStat.HasValue && NumberOfDice == 0)
                return AdditionalDamageBonus == 0 ? "" : $"{AdditionalDamageBonus} damage";

            int damageBonus = GetDamageFromStat(chr) + chr.GetBonuses(eStats.DMG) + AdditionalDamageBonus;

            string damageBonusStr = damageBonus == 0 ? "" : $"{damageBonus:+0;-#}";
            return damageBonusStr;
        }

        int GetDamageFromStat(CharacterModel chr)
        {
            var damage = (DamageStat.HasValue ? chr.GetStatMod(DamageStat.Value) : 0);
            var multiplier = (StrengthAndAHalf ? 1.5 : 1);

            var multipliedDamage = damage * multiplier;

            return (int)multipliedDamage;
        }

        public Attack Clone()
        {
            var clone = (Attack)MemberwiseClone();
            clone.AttackAddons = this.AttackAddons?.Select(x => x.Clone()).ToArray();
            return clone;
        }
    }

    public class AttackAddon : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string Description { get; set; }
        public string Suffix { get; set; }
        public int NumberOfDice { get; set; }
        public int SidesOfDice { get; set; }
        public int AdditionalDamageBonus { get; set; }
        public eStats? DamageStat { get; set; }

        public string DescriptionForAddPopup 
        {
            get
            {
                string description = string.IsNullOrEmpty(Description) ? "" : $"{Description} ";
                string dice = NumberOfDice == 0 ? "" : $"{NumberOfDice}d{SidesOfDice}";
                string plusStat = DamageStat.HasValue ? $"+ {DamageStat}" : "";
                string plusBonus = AdditionalDamageBonus != 0 ? AdditionalDamageBonus.ToString("+0;-#") : "";

                string suffix = string.IsNullOrEmpty(Suffix) ? "" : $" ({Suffix})";

                return $"{description}{dice}{plusStat}{plusBonus}{suffix}";
            }

        }

        public string RenderAdditionalDescription(CharacterModel chr)
        {
            string damageBonus = GetDamageBonus(chr);

            string dice = NumberOfDice == 0 ? "" : $"{NumberOfDice}d{SidesOfDice}";
            string suffix = string.IsNullOrEmpty(Suffix) ? "" : $" ({Suffix})";
            string description = string.IsNullOrEmpty(Description) ? "" : $"{Description} ";

            return $"{description}{dice}{damageBonus}{suffix}";
        }

        string GetDamageBonus(CharacterModel chr)
        {
            if (!DamageStat.HasValue && NumberOfDice == 0)
                return AdditionalDamageBonus == 0 ? "" : $"{AdditionalDamageBonus} damage";

            int damageBonus = (DamageStat.HasValue ? chr.GetStatMod(DamageStat.Value) : 0) + AdditionalDamageBonus;

            string damageBonusStr = damageBonus == 0 ? "" : $"{damageBonus:+0;-#}";
            return damageBonusStr;
        }

        public AttackAddon Clone()
        {
            return (AttackAddon)MemberwiseClone();
        }
    }

    public class RelevantSkill : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string SkillName { get; set; }
        public eStats RelevantStat { get; set; }
        public int BonusFromRanksAndOther { get; set; }
        public bool InClass { get; set; }
        public string ConditionalStuff { get; set; }

        public string RenderToString(CharacterModel chr)
        {
            string conditionalStuff = string.IsNullOrEmpty(ConditionalStuff) ? "" : $"( {ConditionalStuff})";

            int totalBonus = chr.GetStatMod(RelevantStat) + BonusFromRanksAndOther + (InClass ? 3 : 0);

            return $"{totalBonus:+0;-#} {SkillName} ({RelevantStat}){conditionalStuff}";
        }

        public RelevantSkill Clone()
        {
            return (RelevantSkill)MemberwiseClone();
        }
    }
}
