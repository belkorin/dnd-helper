﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dndhelper.models
{
    public class PlayerModel : CharacterModel
    {
        public PlayerModel() { }

        public override CharacterModel Clone()
        {
            return ClonePlayer();
        }

        public override void ApplyClone(CharacterModel clone)
        {
            var playerClone = clone as PlayerModel;
            if (playerClone != null)
                ApplyPlayerClone(playerClone);
            else
                base.ApplyClone(clone);
        }

        public PlayerModel ClonePlayer()
        {
            var clone = (PlayerModel)MemberwiseClone();
            clone.BonusesAndPenalties = this.BonusesAndPenalties?.ToDictionary(x => x.Key, x => x.Value.Select(y => y.Clone()).ToList());
            clone.Attacks = this.Attacks?.Select(x => x.Clone()).ToArray();
            clone.RelevantSkills = this.RelevantSkills?.Select(x => x.Clone()).ToArray();
            clone._stats = this._stats.Select(x => x).ToArray();
            return clone;
        }

        public void ApplyPlayerClone(PlayerModel clone)
        {
            base.ApplyClone(clone);
        }


        public PlayerModel(bool design)
        {
            _stats = new int[] { 15, 14, 13, 16, 12, 11, 3 };
            _bonusesAndPenalties = new Dictionary<eStats, List<StatBonusOrPenalty>>();
            Name = "Totally not Tcharek";
            HP = 42;
            HD = 5;
            CurrentHP = 42;
            ArmorBonus = 4;
            ShieldBonus = 0;
            NaturalArmorBonus = 0;
            DodgeBonus = 1;
            MaxDexToAC = 6;
            SizeCategory = SizeCategories.Medium;
            FortSave = 2;
            RefSave = 6;
            WillSave = 5;
            MoveSpeed = "30'";
            Senses = "Low-light vision";
            Space = 5;
            Reach = 5;

            Notes = @"+4 on saves vs poison
+1 reflex to avoid traps";

            SpecialAbilities = @"Long-Nose Form
You can shift into the form of a human with an unusually long nose.

Inspiration (Ex)
An investigator has the ability to augment skill checks and ability checks through his brilliant inspiration. As a free action, he can expend one use of inspiration from his pool to add 1d6 to the result of that check, including any on which he takes 10 or 20.
";
            Notes = "Wears a plague doctor mask over his beak";

            RelevantSkills = new[]
            {
                new RelevantSkill
                {
                    SkillName = "Acrobatics",
                    InClass = true,
                    BonusFromRanksAndOther = 1,
                    RelevantStat = eStats.DEX
                },
                new RelevantSkill
                {
                    SkillName = "Bluff",
                    InClass = true,
                    BonusFromRanksAndOther = 2,
                    RelevantStat = eStats.CHA
                },
                new RelevantSkill
                {
                    SkillName = "Diplomacy",
                    InClass = true,
                    BonusFromRanksAndOther = 2,
                    RelevantStat = eStats.CHA
                },
                new RelevantSkill
                {
                    SkillName = "Escape Artist",
                    InClass = false,
                    BonusFromRanksAndOther = 0,
                    RelevantStat = eStats.DEX
                },
                new RelevantSkill
                {
                    SkillName = "Intimidate",
                    InClass = true,
                    BonusFromRanksAndOther = 5,
                    RelevantStat = eStats.CHA
                },
                new RelevantSkill
                {
                    SkillName = "Knowledge (Arcana, Dungeon, Nature, Planes, Religion)",
                    InClass = true,
                    BonusFromRanksAndOther = 2,
                    RelevantStat = eStats.INT
                },
                new RelevantSkill
                {
                    SkillName = "Knowledge (Engineer, Geography, History, Local, Nobility)",
                    InClass = true,
                    BonusFromRanksAndOther = 1,
                    RelevantStat = eStats.INT
                },
                new RelevantSkill
                {
                    SkillName = "Perception",
                    InClass = true,
                    BonusFromRanksAndOther = 4,
                    RelevantStat = eStats.WIS,
                    ConditionalStuff = "+2 when finding traps"
                },
                new RelevantSkill
                {
                    SkillName = "Swim",
                    InClass = false,
                    BonusFromRanksAndOther = 2,
                    RelevantStat = eStats.STR
                }
            };

            Attacks = new[]
            {
                new Attack
                {
                    Description = "Bite",
                    ToHitStat = eStats.STR,
                    NumberOfDice = 1,
                    SidesOfDice = 3,
                    DamageStat = eStats.STR
                },
                new Attack
                {
                    Description = "Falcata (2 hand)",
                    ToHitStat = eStats.STR,
                    NumberOfDice = 1,
                    SidesOfDice = 8,
                    DamageStat = eStats.STR,
                    StrengthAndAHalf = true,
                    Suffix = "Crit 19-20/x3",
                    AdditionalSuffix = "cold iron"

                },
                new Attack
                {
                    Description = "Falcata (1 hand)",
                    ToHitStat = eStats.STR,
                    NumberOfDice = 1,
                    SidesOfDice = 8,
                    DamageStat = eStats.STR,
                    Suffix = "Crit 19-20/x3",
                    AdditionalSuffix = "cold iron"

                },
            };
        }
    }

    public class DesignTimePlayerModel : PlayerModel
    {
        public DesignTimePlayerModel() : base(true) { }
    }
}
