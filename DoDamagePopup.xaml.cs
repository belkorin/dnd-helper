﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dndhelper
{
    /// <summary>
    /// Interaction logic for DoDamagePopup.xaml
    /// </summary>
    public partial class DoDamagePopup : Window
    {
        public bool BypassDR { get; private set; }
        public int Damage { get; private set; }

        public DoDamagePopup()
        {
            InitializeComponent();
        }

        private void btnDialogOk_Click(object sender, RoutedEventArgs e)
        {
            Damage = txtAnswer.Value ?? 0;
            BypassDR = bypassDR.IsChecked ?? false;

            this.DialogResult = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
