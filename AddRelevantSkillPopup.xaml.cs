﻿using dndhelper.models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dndhelper
{
    /// <summary>
    /// Interaction logic for AddRelevantSkillPopup.xaml
    /// </summary>
    public partial class AddRelevantSkillPopup : Window
    {
        public RelevantSkill RelevantSkill { get; private set; }

        public AddRelevantSkillPopup()
        {
            RelevantSkill = new RelevantSkill();
            DataContext = RelevantSkill;

            InitializeComponent();
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
