﻿using dndhelper.models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dndhelper
{
    /// <summary>
    /// Interaction logic for AddAttackAddonPopup.xaml
    /// </summary>
    public partial class AddAttackAddonPopup : Window
    {
        public AttackAddon AttackAddon { get; private set; }

        public AddAttackAddonPopup()
        {
            AttackAddon = new AttackAddon();
            DataContext = AttackAddon;

            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
