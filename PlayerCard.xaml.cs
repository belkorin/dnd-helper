﻿using dndhelper.models;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace dndhelper
{
    /// <summary>
    /// Interaction logic for PlayerCard.xaml
    /// </summary>
    public partial class PlayerCard : UserControl
    {
        public MainModel MainModel
        {
            get { return (MainModel)this.GetValue(StateProperty); }
            set { this.SetValue(StateProperty, value); }
        }
        public static readonly DependencyProperty StateProperty = DependencyProperty.Register(
          "MainModel", typeof(MainModel), typeof(PlayerCard), new PropertyMetadata(null));

        public PlayerCard()
        {
            InitializeComponent();
        }

        public PlayerCard(MainModel mainModel)
        {
            MainModel = mainModel;
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            var charModel = DataContext as CharacterModel;

            var clone = charModel.Clone();

            var popup = new CreateCreature(clone);

            if (popup.ShowDialog() != true)
                return;

            charModel.ApplyClone(clone);
        }

        private void DoDamage_Click(object sender, RoutedEventArgs e)
        {
            var model = DataContext as CharacterModel;

            if (model == null)
                return;

            var popup = new DoDamagePopup();
            if (popup.ShowDialog() != true)
                return;

            model.DoDamageOrHeal(popup.BypassDR, popup.Damage);
        }

        private void RemoveModifier_Click(object sender, RoutedEventArgs e)
        {
            var model = DataContext as CharacterModel;

            if (model == null)
                return;

            var button = (Button)sender;
            var modifier = (StatBonusOrPenalty)button.Tag;

            model.RemoveSpecificBonus(modifier);
        }

        private void AddModifier_Click(object sender, RoutedEventArgs e)
        {
            var model = DataContext as CharacterModel;

            if (model == null)
                return;

            var popup = new AddModifierPopup();
            if (popup.ShowDialog() != true)
                return;

            var mod = popup.Modifier;

            model.SetSpecificBonus(mod.Modifies, mod.BonusSource, mod.Amount, mod.Description, mod.Duration, mod.StartedOnRound);
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<CharacterModel> collection = null;

            var model = DataContext as CharacterModel;

            if (model == null)
                return;

            var result1 = MessageBox.Show($"Are you sure you want to remove {model.Name}?", "confirm...", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result1 != MessageBoxResult.Yes)
                return;

            var result2 = MessageBox.Show($"Do you want to save {model.Name} before deleting?", "save?", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

            if (result2 == MessageBoxResult.Yes)
            {
                try
                {
                    var json = JsonConvert.SerializeObject(model);
                    SaveFileDialog saveFileDialog = new SaveFileDialog() { DefaultExt = ".json" };
                    if (saveFileDialog.ShowDialog() == true)
                        File.WriteAllText(saveFileDialog.FileName, json);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Couldn't save. \r\n{ex.Message}");
                    return;
                }
            }
            else if (result2 != MessageBoxResult.No)
                return;

            if (MainModel.Friends.Contains(model))
                collection = MainModel.Friends;
            else
                collection = MainModel.Foes;

            collection.Remove(model);
        }
    }
}
