﻿using dndhelper.models;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace dndhelper
{
    /// <summary>
    /// Interaction logic for CreateCreature.xaml
    /// </summary>
    public partial class CreateCreature : Window
    {
        public CharacterModel Model;
        bool _isNew;

        public CreateCreature()
        {
            Model = new CharacterModel();
            DataContext = Model;
            _isNew = true;

            InitializeComponent();

            EventManager.RegisterClassHandler(typeof(TextBox),
                                  TextBox.GotFocusEvent,
                                 new RoutedEventHandler(this.TextBox_GotFocus));
        }

        public CreateCreature(CharacterModel model)
        {
            Model = model;
            DataContext = Model;
            _isNew = false;

            InitializeComponent();

            EventManager.RegisterClassHandler(typeof(TextBox),
                                  TextBox.GotFocusEvent,
                                 new RoutedEventHandler(this.TextBox_GotFocus));
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (_isNew)
                Model.CurrentHP = Model.HP;

            Model.ApplyPending();

            SaveFileDialog saveFileDialog = new SaveFileDialog() { DefaultExt = ".json" };
            if (saveFileDialog.ShowDialog() == true)
            {
                Model.FileName = saveFileDialog.FileName;
                var json = JsonConvert.SerializeObject(Model);
                File.WriteAllText(saveFileDialog.FileName, json);
                this.DialogResult = true;
                this.Close();
            }
        }

        private void AddWithoutSave_Click(object sender, RoutedEventArgs e)
        {
            if (_isNew)
                Model.CurrentHP = Model.HP;

            Model.ApplyPending();

            this.DialogResult = true;
            this.Close();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog() { DefaultExt = ".json" };
            if(openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var text = File.ReadAllText(openFileDialog.FileName);
                    var model = JsonConvert.DeserializeObject<CharacterModel>(text);

                    Model = model;
                    DataContext = model;

                    Model.FileName = openFileDialog.FileName;

                    this.DialogResult = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Something went wrong: {ex}");
                }
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void AddMeleeAttack_Click(object sender, RoutedEventArgs e)
        {
            AddAttack(false);
        }

        private void AddRangedAttack_Click(object sender, RoutedEventArgs e)
        {
            AddAttack(true);
        }

        private void AddAttack(bool isRanged)
        {
            var popup = new AddAttackPopup(isRanged);

            if (popup.ShowDialog() != true)
                return;

            Model.Attacks = Model.Attacks.Concat(new[] { popup.Attack }).ToArray();
        }

        private void AddSkill_Click(object sender, RoutedEventArgs e)
        {
            var popup = new AddRelevantSkillPopup();

            if (popup.ShowDialog() != true)
                return;

            Model.RelevantSkills = Model.RelevantSkills.Concat(new[] { popup.RelevantSkill }).ToArray();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            var textBox = sender as TextBox;
            textBox.SelectAll();
        }
    }
}
